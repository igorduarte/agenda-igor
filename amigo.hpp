#ifndef AMIGO_H
#define AMIGO_H

#include "pessoa.hpp"
#include <string>

#include <iostream>
using namespace std;

class Amigo : public Pessoa{
	private:
		string facebook;
		string telefoneFixo;
		string email;
	public:
		Amigo();
		Amigo(string facebook, string telefoneFixo, string email);
		
		void setFacebook(string facebook);
		string getFacebook();

		void setTelefoneFixo(string telefoneFixo);
		string getTelefoneFixo();

		void setEmail(string email);
		string getEmail();
};

#endif 
