#ifndef CONTATO_H
#define CONTATO_H

#include "pessoa.hpp"
#include <iostream>
#include <string>

using namespace std;

class Contato : public Pessoa{
	private:
		string endereco;

	public:
		Contato();
		Contato(string endereco);

		void setEndereco(string endereco);
		string getEndereco();
};

#endif
