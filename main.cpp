#include <iostream>
#include "pessoa.hpp"
#include "amigo.hpp"
#include "contato.hpp"
#include <string>

using namespace std;

int main() {
	
	Amigo amigo;
	Contato contato;
		
	amigo.setNome("Igor");
	amigo.setIdade("17");
	amigo.setTelefone("9848-5832");
	amigo.setFacebook("Igor Ribeiro");
	amigo.setTelefoneFixo("3331-6104");
	amigo.setEmail("igor.ribeiro.duarte@gmail.com");

	cout << "              Amigo           " << endl;
	cout << "Nome: " << amigo.getNome() << endl;
	cout << "Idade: " << amigo.getIdade() << endl;
	cout << "Telefone: " << amigo.getTelefone() << endl;
	cout << "Facebook: " << amigo.getFacebook() << endl;
	cout << "Telefone Fixo: " << amigo.getTelefoneFixo() << endl;
	cout << "Email: " << amigo.getEmail() << endl;

	cout << "==============================================" << endl;

	cout << "              Contato            " << endl;
	contato.setNome("Augusto");
	contato.setIdade("12");
	contato.setTelefone("5555-5555");
	contato.setEndereco("Recanto das Emas - Quadra 555, Conjunto: 55, Casa: 5");

	cout << "Nome: " << contato.getNome() << endl;
	cout << "Idade: " << contato.getIdade() << endl;
	cout << "Telefone: " << contato.getTelefone() << endl;
	cout << "Endereco: " << contato.getEndereco() << endl;

	return 0;
}
