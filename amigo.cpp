#include "amigo.hpp"
#include <string>
#include <iostream>

using namespace std;

Amigo::Amigo(){
	string facebook="vazio";
	string telefoneFixo="vazio";
	string email="vazio";
}

Amigo::Amigo(string facebook, string telefoneFixo, string email){
	this->facebook=facebook;
	this->telefoneFixo=telefoneFixo;
	this->email=email;
}

void Amigo::setFacebook(string facebook){
	this->facebook=facebook;
}

void Amigo::setTelefoneFixo(string telefoneFixo){
	this->telefoneFixo=telefoneFixo;
}

void Amigo::setEmail(string email){
	this->email=email;
}

string Amigo::getFacebook(){
	return facebook;
}

string Amigo::getTelefoneFixo(){
	return telefoneFixo;
}

string Amigo::getEmail(){
	return email;
}
